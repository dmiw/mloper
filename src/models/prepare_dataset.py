import click
import pandas as pd
from typing import List
import logging

FEATURES = ['VendorID', 'passenger_count', 'trip_distance', 'RatecodeID',
            'store_and_fwd_flag', 'PULocationID', 'DOLocationID', 'payment_type',
            'fare_amount', 'extra', 'mta_tax', 'tip_amount', 'tolls_amount',
            'improvement_surcharge', 'total_amount', 'congestion_surcharge',
            'airport_fee', 'PU_DO', 'pickup_weekday', 'pickup_hour_weekofyear',
            'pickup_hour', 'pickup_minute', 'pickup_dt', 'pickup_week_hour']


@click.command()
@click.argument("input_filepath", type=click.Path(exists=True))
@click.argument("output_file_paths", type=click.Path(), nargs=2)
def prepare_datasets(input_filepath, output_file_paths: List[str]):
    logger = logging.getLogger(__name__)
    logger.info('Creating train and test datasets..')

    df = pd.read_parquet(input_filepath)
    df = df[FEATURES]
    df = df.drop_duplicates()

    train = df.sample(frac=0.75, random_state=200)
    test = df.drop(train.index)

    train.to_csv(output_file_paths[0], index=False)
    test.to_csv(output_file_paths[1], index=False)


if __name__ == "__main__":
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    prepare_datasets()
