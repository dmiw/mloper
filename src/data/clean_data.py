# -*- coding: utf-8 -*-
import logging
from pathlib import Path

import click
# import great_expectations as ge
# import pandera as pa
import numpy as np
import pandas as pd
import yaml
from dotenv import find_dotenv, load_dotenv

load_dotenv(find_dotenv())

MIN_AREA = 15  # Outlier range for floor area
MAX_AREA = 300

MIN_PRICE = 1_000_000  # Outlier range for price
MAX_PRICE = 100_000_000

MIN_KITCHEN = 3  # Outlier range for kitchen area
MAX_KITCHEN = 70


@click.command()
@click.argument('input_filepath', type=click.Path(exists=True))
@click.argument('output_filepath', type=click.Path())
def clean_data(input_filepath: str, output_filepath: str):
    """Clean data

    Args:
        input_filepath (str): Path to csv file
        output_path (str): Path to data/interim for cleaned data
    """
    logger = logging.getLogger(__name__)
    logger.info('Cleaning data...')

    df = pd.read_parquet(input_filepath)
    df['store_and_fwd_flag'] = df['store_and_fwd_flag'].map({'N': 0, 'Y': 1})

    df.to_parquet(output_filepath, index=False)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)
    # project_dir = Path(__file__).resolve().parents[2]
    load_dotenv(find_dotenv())
    
    clean_data()
