import datetime as dt
import logging
from datetime import timedelta

import click
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import xgboost as xgb
from dotenv import find_dotenv, load_dotenv

plt.rcParams['figure.figsize'] = [16, 10]
import warnings

warnings.filterwarnings('ignore')
load_dotenv(find_dotenv())


@click.command()
@click.argument("input_path", type=click.Path())
@click.argument("output_path", type=click.Path())
def add_features(input_path: str, output_path: str):
    """Add features

    Args:
        input_path (str): Path to csv file
        output_path (str): Path to data/interim for data with new features
    """
    logger = logging.getLogger(__name__)
    logger.info('Feature engineering...')

    df = pd.read_parquet(input_path)
    
    df['duration'] = (df.tpep_dropoff_datetime - df.tpep_pickup_datetime).dt.total_seconds() // 60
    df = df[(df.duration >= 1) & (df.duration <= 60)]
    df['PU_DO'] = df['PULocationID'].astype(str) + '_' + df['DOLocationID'].astype(str)

    # Datetime features
    df.loc[:, 'pickup_weekday'] = df['tpep_pickup_datetime'].dt.weekday
    df.loc[:, 'pickup_hour_weekofyear'] = df['tpep_pickup_datetime'].dt.weekofyear
    df.loc[:, 'pickup_hour'] = df['tpep_pickup_datetime'].dt.hour
    df.loc[:, 'pickup_minute'] = df['tpep_pickup_datetime'].dt.minute
    df.loc[:, 'pickup_dt'] = (df['tpep_pickup_datetime'] - df['tpep_pickup_datetime'].min()).dt.total_seconds()
    df.loc[:, 'pickup_week_hour'] = df['pickup_weekday'] * 24 + df['pickup_hour']

    df.to_parquet(output_path, index=False)


if __name__ == "__main__":
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    load_dotenv(find_dotenv())
    
    add_features()

